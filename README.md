#  Создание окружения master & Development cluster with terraform
1. копируем переменные и место хранения состояния кластера
 - cp vars/development ./variables.auto.tfvars
 - cp vars/development.tf ./terraform.tf
2. инициализируем `terraform init`
3. запускаем `terraform validate` - валидируем конфигурацию
4. запускаем `terraform plan -out "<name artifact>"` - сохраняем артифакт плана, для разных окружения используя разные branch
5. применяя полученный plan запускаем `terraform apply "<name artifact>"` - операция запускаем после нажатия руками apply
6. если нам не нужен кластер, 4м шагом CI мы можем удалить кластер.

## [Terraform init](https://www.padok.fr/en/blog/kubernetes-google-cloud-terraform-cluster)
### включение доступа api в GCP, enable apis in gcp
 - env |
   - <project_name> = curse-work
   - <service_account_name> = terraform-andrew
 -  установить нужные пакеты, и сделать gcloud init
 ```
echo '#!/bin/bash
gcloud services enable compute.googleapis.com
gcloud services enable servicenetworking.googleapis.com
gcloud services enable cloudresourcemanager.googleapis.com
gcloud services enable container.googleapis.com' > gcloud_en_api.sh && chmod +x ./gcloud_en_api.sh && \
./gcloud_en_api.sh
 ```
### создать сервис аккаунт (create service account)
   - `gcloud iam service-accounts create terraform-andrew`
   - вывод Created service account [terraform-andrew].
### дать grant permition to some roles (create grant some permission in gcp)
 ```
gcloud projects add-iam-policy-binding <project_name> --member serviceAccount:<service_account_name>@<project_name>.iam.gserviceaccount.com --role roles/container.admin
gcloud projects add-iam-policy-binding <project_name> --member serviceAccount:<service_account_name>@<project_name>.iam.gserviceaccount.com --role roles/compute.admin
gcloud projects add-iam-policy-binding <project_name> --member serviceAccount:<service_account_name>@<project_name>.iam.gserviceaccount.com --role roles/iam.serviceAccountUser
gcloud projects add-iam-policy-binding <project_name> --member serviceAccount:<service_account_name>@<project_name>.iam.gserviceaccount.com --role roles/resourcemanager.projectIamAdmin
 ```
### создать и скачать key который позволяет аутентироваться как сервис аккаунт(dnowload accont key for project)
  ```
gcloud iam service-accounts keys create terraform_key.json --iam-account: terraform-andrew@curse-work.iam.gserviceaccount.com
gcloud iam service-accounts keys create --iam-account my-iam-account@somedomain.com key.json
  ```
### Terraform state in Google Cloud Storage для хранения кода состояния нашей инфраструктуры на удаленном хранилище(отдельный конфиг для разных окружений)
 - можно создать хранилище в [графической консоли](https://console.cloud.google.com/storage/create-bucket), а можно, с помощью команды
   - `gsutil mb -p curse-work -c regional -l EUROPE-WEST3 gs://terraform_bucket1/`
   - включаем поддержку версионирования
    - `gsutil versioning set on gs://terraform_bucket1/`
   - даем разрешение нашему аккаунту на чтение/запись 
    - `gsutil iam ch serviceAccount:terraform-andrew@curse-work.iam.gserviceaccount.com:legacyBucketWriter gs://terraform_bucket1/`
   - создаем terraform.tr фаил, чтобы использовать этот аккаунт c баккетом
### создаем структуру для использования terraform
 ```
├── creds
│   └── serviceaccount.json
├── main.tf
├── planfile
├── providers.tf
├── README1.md
├── README.md
├── terraform_key___.json
├── variables.tf
└── vars
    ├── development
    ├── development.tf
    ├── master
    └── master.tf
 ```