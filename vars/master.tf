terraform {
  backend "gcs" {
    credentials = "creds/serviceaccount.json"
    bucket      = "terraform_bucket_prod"
    prefix      = "terraform/state"
  }
}