terraform {
  backend "gcs" {
    credentials = "creds/serviceaccount.json"
    bucket      = "terraform_bucket1"
    prefix      = "terraform/state"
  }
}