resource google_container_cluster "cluster" {
  provider                 = google-beta
  name                     = var.name
  location                 = var.region
  remove_default_node_pool = var.remove_default_node_pool
  initial_node_count       = var.initial_node_count
  monitoring_service       = "none"
  logging_service          = "none"
  min_master_version       = "1.15"
  node_locations           = var.zones

  network_policy {
    enabled = true
    provider = "CALICO"
  }
  // temporarily disable istio
  addons_config {
    istio_config {
      disabled = var.istio_disabled
    }
  }
  maintenance_policy {
    daily_maintenance_window {
      start_time = "02:00"
    }
  }
}

resource google_container_node_pool "node_pool" {
  name               = var.name
  location           = var.region
  cluster            = google_container_cluster.cluster.name
  version            = "1.15.11-gke.5"
  initial_node_count = var.initial_node_count_node_pool
  # node_count         = var.node_count

  autoscaling {
    min_node_count = var.min_count
    max_node_count = var.max_count
  }

  node_config {
    preemptible  = true
    machine_type = var.machine_type
    disk_size_gb = var.disk_size_gb

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/monitoring.write",
    ]
  }

  timeouts {
    create = "60m"
    update = "60m"
    delete = "60m"
  }
}

resource google_container_node_pool "node_pool_infra" {
  name               = "gke-cluster-infra"
  location           = var.region
  cluster            = google_container_cluster.cluster.name
  version            = "1.15.11-gke.5"
  initial_node_count = "1"
  # node_count         = var.node_count

  autoscaling {
    min_node_count = var.min_count
    max_node_count = var.max_count
  }

  node_config {
    preemptible  = true
    machine_type = "n1-standard-1"
    disk_size_gb = var.disk_size_gb

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/monitoring.write",
    ]
    taint {
      key    = "node-role" 
      value  = "infra" 
      effect = "NO_SCHEDULE"
    }
  }

  timeouts {
    create = "60m"
    update = "60m"
    delete = "60m"
  }
}

resource google_compute_address "address" {
  name         = var.name
  address_type = "EXTERNAL"
}






# module "gke" {
#   source                     = "terraform-google-modules/kubernetes-engine/google"
#   version                    = "8.1.0"
#   project_id                 = "${var.project_id}"
#   name                       = "${var.name}"
#   region                     = "${var.region}"
#   zones                      = "${var.zones}"
#   network                    = "default"
#   subnetwork                 = "default"
#   ip_range_pods              = ""
#   ip_range_services          = ""
#   http_load_balancing        = false
#   horizontal_pod_autoscaling = true
#   #kubernetes_dashboard       = var.kubernetes_dashboard
#   network_policy             = true
#   logging_service            = "none"
#   monitoring_service         = "none"
#   #remove_default_node_pool   = "${var.remove_default_node_pool}"
  
#   node_pools = [
#     {
#       name               = "default-node-pool"
#       machine_type       = var.machine_type
#       min_count          = var.min_count
#       max_count          = var.max_count
#       disk_size_gb       = var.disk_size_gb
#       disk_type          = "pd-standard"
#       image_type         = "COS"
#       auto_repair        = true
#       auto_upgrade       = true
#       service_account    = var.service_account
#       preemptible        = false
#       initial_node_count = var.initial_node_count
#       node_count         = var.node_count
#     },
#   ]

#   node_pools_oauth_scopes = {
#     all = []

#     default-node-pool = [
#       "https://www.googleapis.com/auth/cloud-platform",
#     ]
#   }

#   node_pools_labels = {
#     all = {}

#     default-node-pool = {
#       default-node-pool = true
#     }
#   }

#   node_pools_metadata = {
#     all = {}

#     default-node-pool = {
#       node-pool-metadata-custom-value = "my-node-pool"
#     }
#   }

# #   node_pools_taints = {
# #     all = []

# #     default-node-pool = [
# #       {
# #         key    = "default-node-pool"
# #         value  = true
# #         effect = "PREFER_NO_SCHEDULE"
# #       },
# #     ]
# #   }

#   node_pools_tags = {
#     all = []

#     default-node-pool = [
#       "default-node-pool",
#     ]
#   }
# }
